<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>
<body>
<div id="container">
	<div id="header">
		<a href="<?php print check_url($base_path); ?>"><?php if ($logo) { print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />'; } ?></a>	
		<h2><a href="<?php print check_url($base_path); ?>"><?php print check_plain($site_name); ?></a> <?php print check_plain($site_slogan); ?></h2>
		<div id="meta-nav">
			<ul>
			<?php if (is_array($secondary_links)) : ?>
			<?php foreach($secondary_links AS $links){ echo '<li class="page_item">'. l($links['title'], $links['href']). '</li>'; }
			endif; ?>
			</ul>
		</div>
		<ul id="nav">
			<?php if (is_array($primary_links)) : ?>
			<?php foreach($primary_links AS $links){ echo '<li class="page_item">'. l($links['title'], $links['href']). '</li>'; }
			endif; ?>
		</ul>
		<hr>
		<div id="search">
            <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>			
		</div> 
	</div>
	<div id="content-wrap">
		<div id="content">
			<?php if ($mission != ""): ?>
			<div id="mission"><?php print $mission ?></div>
			<?php endif; ?>
			<?php if ($breadcrumb): print $breadcrumb; endif; ?>
			<?php if ($title != ""): ?>
			<h1><?php print $title ?></h1>
			<?php endif; ?>						
			<?php print $header; ?>					
			<?php if ($tabs != ""): ?>
			<?php print $tabs ?>
			<?php endif; ?>						
			<?php if ($help != ""): ?>
			<p id="help"><?php print $help ?></p>
			<?php endif; ?>
			<?php if ($messages != ""): ?>
			<div id="message"><?php print $messages ?></div>
			<?php endif; ?>
			<?php print($content) ?>			
		</div>	
		<div id="sidebar">
			<?php if ($sidebar_left): ?>
			<?php print $sidebar_left ?>
			<?php endif; ?>
			<?php if ($sidebar_right): ?>
			<?php print $sidebar_right ?>
			<?php endif; ?>
		</div>
	</div>
</div>
    <hr>
    <div id="footer-outer">
		<div id="footer">
			<?php print $footer_message ?>
			<p class="copy"><a href="http://www.ihom.ru">tech</a></p>
        </div>
	</div>
</body></html>