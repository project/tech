<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2> 
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print t('!date — !username', array('!username' => theme('username', $node), '!date' => format_date($node->created))); ?></span>
  <?php endif; ?>
  
    <div class="clear-block clear">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms"><?php print $terms ?></div>
    <?php endif;?>
    </div>

  <div class="content">
    <?php print $content ?>
  </div>

    <?php if ($links): ?>
      <div class="smlink"><?php print $links; ?></div>
    <?php endif; ?>
	
	<?php if ($leech_news_item): ?>
      <div class="parser"><a href="<?php print $leech_news_item->link ?>" title="<?php print $title ?>"><?php $link_name = parse_url($leech_news_item->link); print($link_name['host']); ?></a></div>
    <?php endif; ?>
	
  </div>	
</div>