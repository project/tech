<div id="<?php print 'block-'.$block->module ?>">

<?php if ($block->subject): ?>
  <h3 id="subnav"><?php print $block->subject ?></h3>
<?php endif;?>

  <div id="subnav-wrap"><?php print $block->content ?></div>
</div>